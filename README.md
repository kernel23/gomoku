Le Jeux Gomoku :
================

Description :
=============
Ce document intitulé « Jeu de gomoku avec ia » mise en place du jeu de Gomoku dans une interface graphique.
Le but du jeu est d'aligner 5 pions plus rapidement que son adversaire.

La compilation :
================

`mkdir build`
`cmake ..`
`make`


Lancer le serveur Web : 
=======================

./gomoku-server --docroot . --http-address 0.0.0.0 --http-port 3000`


