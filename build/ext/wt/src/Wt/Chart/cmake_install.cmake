# Install script for directory: /home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Chart" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WScatterData"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChartGlobal"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChart2DImplementation"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WStandardChartProxyModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WPieChart"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WLegend"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WCartesian3DChart"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAxis"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WCartesianChart"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WDataSeries"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractGridData"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WSelection"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChart3DImplementation"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WGridData"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WStandardColorMap"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractColorMap"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChartPalette"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractChartImplementation"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WStandardPalette"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractChart"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractDataSeries3D"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WLegend3D"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractChartModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WEquidistantGridData"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAxisSliderWidget"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Chart" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WPieChart.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractColorMap.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WCartesianChart.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WSelection.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChart3DImplementation.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WStandardChartProxyModel.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WEquidistantGridData.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChart2DImplementation.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WScatterData.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WDataSeries.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WLegend3D.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WGridData.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractGridData.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAxis.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractChart.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractChartModel.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractChartImplementation.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChartGlobal.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WStandardPalette.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WCartesian3DChart.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAbstractDataSeries3D.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WChartPalette.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WStandardColorMap.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WLegend.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Chart/WAxisSliderWidget.h"
    )
endif()

