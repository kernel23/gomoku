# Install script for directory: /home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwtdbo.so.4.0.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwtdbo.so.49"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwtdbo.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/libwtdbo.so.4.0.4"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/libwtdbo.so.49"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/libwtdbo.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwtdbo.so.4.0.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwtdbo.so.49"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwtdbo.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-dbo.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-dbo.cmake"
         "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/CMakeFiles/Export/lib/cmake/wt/wt-target-dbo.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-dbo-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-dbo.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/CMakeFiles/Export/lib/cmake/wt/wt-target-dbo.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/CMakeFiles/Export/lib/cmake/wt/wt-target-dbo-relwithdebinfo.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/Wt/WConfig.h")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Dbo" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Query.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/DbAction.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlConnectionPool.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr_tuple.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Exception.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Field_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/EscapeOStream.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Call.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlStatement.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Session.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryModel_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Session_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Json.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Query_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlConnection.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/DbAction_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/StdSqlTraits.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/WtSqlTraits.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/FixedSqlConnectionPool.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Call_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryModel.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/WDboDllDefs.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/weak_ptr.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/collection.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/weak_ptr_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryColumn.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/collection_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Field.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Dbo.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlTraits_impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Impl.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/StringStream.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Types.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Transaction.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlTraits.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Dbo" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/FixedSqlConnectionPool"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/WtSqlTraits"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/weak_ptr"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Session"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Query"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlConnectionPool"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Transaction"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlConnection"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/collection"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Dbo"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Field"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Types"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr_tuple"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/StdSqlTraits"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Impl"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlTraits"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Call"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlStatement"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Exception"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/DbAction"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Json"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryColumn"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Dbo" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/FixedSqlConnectionPool"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/WtSqlTraits"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/weak_ptr"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Session"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Query"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlConnectionPool"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Transaction"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlConnection"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/collection"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Dbo"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Field"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Types"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr_tuple"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/StdSqlTraits"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Impl"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlTraits"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Call"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/ptr"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/SqlStatement"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Exception"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/DbAction"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/Json"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Dbo/QueryColumn"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Dbo/backend/cmake_install.cmake")

endif()

