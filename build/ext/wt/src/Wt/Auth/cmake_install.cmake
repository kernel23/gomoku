# Install script for directory: /home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Auth" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AbstractPasswordService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AbstractUserDatabase.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthModel.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthUtils.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthWidget.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/FacebookService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/FormBaseModel.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/GoogleService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/HashFunction.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/Identity.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/IssuedToken.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/Login.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/LostPasswordWidget.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthAuthorizationEndpointProcess.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthClient.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthTokenEndpoint.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthWidget.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OidcService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OidcUserInfoEndpoint.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordHash.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordPromptDialog.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordService.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordStrengthValidator.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordVerifier.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/RegistrationModel.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/RegistrationWidget.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/Token.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/UpdatePasswordWidget.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/User.h"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/WAuthGlobal.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Auth" TYPE FILE FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OidcService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/Identity"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/HashFunction"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OidcUserInfoEndpoint"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/RegistrationWidget"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/RegistrationModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/Token"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordPromptDialog"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthAuthorizationEndpointProcess"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthTokenEndpoint"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/FacebookService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/IssuedToken"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordVerifier"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthClient"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/Login"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/LostPasswordWidget"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AuthWidget"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/User"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AbstractUserDatabase"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/GoogleService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/WAuthGlobal"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/OAuthWidget"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordStrengthValidator"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/FormBaseModel"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/AbstractPasswordService"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/UpdatePasswordWidget"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Wt/Auth" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src/Wt/Auth/PasswordHash")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/Auth/Dbo/cmake_install.cmake")

endif()

