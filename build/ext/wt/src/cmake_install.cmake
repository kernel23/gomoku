# Install script for directory: /home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwttest.so.4.0.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwttest.so.19"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwttest.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/libwttest.so.4.0.4"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/libwttest.so.19"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/libwttest.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwttest.so.4.0.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwttest.so.19"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwttest.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src:"
           NEW_RPATH "")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-test.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-test.cmake"
         "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/CMakeFiles/Export/lib/cmake/wt/wt-target-test.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-test-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-test.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/CMakeFiles/Export/lib/cmake/wt/wt-target-test.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/CMakeFiles/Export/lib/cmake/wt/wt-target-test-relwithdebinfo.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwt.so.4.0.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwt.so.49"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwt.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/libwt.so.4.0.4"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/libwt.so.49"
    "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/libwt.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwt.so.4.0.4"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwt.so.49"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libwt.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-wt.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-wt.cmake"
         "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/CMakeFiles/Export/lib/cmake/wt/wt-target-wt.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-wt-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/wt/wt-target-wt.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/CMakeFiles/Export/lib/cmake/wt/wt-target-wt.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Rr][Ee][Ll][Ww][Ii][Tt][Hh][Dd][Ee][Bb][Ii][Nn][Ff][Oo])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/wt" TYPE FILE FILES "/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/CMakeFiles/Export/lib/cmake/wt/wt-target-wt-relwithdebinfo.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xx" OR NOT CMAKE_INSTALL_COMPONENT)
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/WtInstall.cmake")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/web/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/Wt/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/isapi/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/fcgi/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/src/http/cmake_install.cmake")

endif()

