# Install script for directory: /home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/examples

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/blog/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/charts/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/chart3D/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/codeview/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/composer/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/dialog/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/dragdrop/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/filetreetable/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/filedrop/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/form/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/gitmodel/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/hangman/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/hello/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/javascript/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/mandelbrot/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/mission/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/onethread/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/painting/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/planner/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/qrlogin/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/simplechat/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/style/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/te-benchmark/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/treelist/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/treeview/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/treeview-dragdrop/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/webgl/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/widgetgallery/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/wt-homepage/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/wtwithqt/cmake_install.cmake")

endif()

