# Install script for directory: /home/zaakaria/Bureau/ProjetC++GL/gomoku/ext/wt/examples/feature

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/auth1/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/auth2/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/broadcast/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/client-ssl-auth/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/dbo/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/locale/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/mediaplayer/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/miniwebgl/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/multiple_servers/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/oauth/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/oidc/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/paypal/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/postall/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/scrollvisibility/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/serverpush/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/socketnotifier/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/suggestionpopup/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/template-fun/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/urlparams/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/video/cmake_install.cmake")
  include("/home/zaakaria/Bureau/ProjetC++GL/gomoku/build/ext/wt/examples/feature/widgetset/cmake_install.cmake")

endif()

