#include "AppGomoku.hpp"
#include "BoardWidget.hpp"
#include "Board.hpp"
#include <string>
#include <Wt/WContainerWidget.h>
#include <Wt/WPushButton.h>
#include <Wt/WTemplate.h>
#include <Wt/WStandardItemModel.h>
#include <Wt/WStandardItem.h>

#include <chrono>

auto now = std::chrono::system_clock::now;

using namespace std;
using namespace Wt;

const string APP_GOMOKU_HTML = R"(
    <h1> Gomoku </h1> <hr />
   <div class="bloc">
       <div class="infosJeu">
           <p>${Joueur-actuel} </p>
           <p>${Gagnant}</p>
           <p>	${button-rejouer} </p>
           <p> ${stat-pie} </p>
       </div>
       <div>${board-widget}</div>
   </div> <hr />
    <p>	${button-stats} </p>
)";

AppGomoku::AppGomoku(const WEnvironment & env) : WApplication(env),_board(5,7){
    _session.setConnection(make_unique<Dbo::backend::Sqlite3>("toto.db"));
    _session.mapClass<GameResult>("gameresult");
    try {
        _session.dropTables();
    }
    catch(Dbo::Exception &e) {
        cerr << "Dbo::Exception: " << e.what() << endl;
    }

    try {

        _session.createTables();
    }
    catch (Dbo::Exception & e) {
        cerr << "Dbo::Exception: " << e.what() << endl;
    }


    auto & css = instance()->styleSheet();
    css.addRule("body", R"(background-color: #D3D3D3)");
    css.addRule(".bloc",R"(display: inline-flex)");
    css.addRule(".infosJeu",R"(min-width: 300px)");

    // add html template
    auto t = root()->addWidget(make_unique<WTemplate>(APP_GOMOKU_HTML));

    // bind html elements to c++ variables

    _joueuractuel = t->bindWidget("Joueur-actuel", make_unique<WText>());

    _gagnant = t->bindWidget("Gagnant", make_unique<WText>());

    _boardWidget = t->bindWidget("board-widget", make_unique<BoardWidget>(*this,_board));

    // connect elements to callback functions
    auto restartButton = t->bindWidget("button-rejouer", make_unique<WPushButton>("Rejouer"));
    restartButton->clicked().connect(this, &AppGomoku::restart);

    auto statsButton = t->bindWidget("button-stats", make_unique<WPushButton>("Afficher les stats"));
    statsButton->clicked().connect(this, &AppGomoku::afficherStats);
    _joueuractuel->setText(WString("Joueur actuel: Joueur blanc"));
    _gagnant->setText(WString("Gagnant: None"));
    // creation de le pie
    auto _model = createModel();
    _statsPie = t->bindWidget("stat-pie", make_unique<Chart::WPieChart>());
    _statsPie->setModel(_model);       
    _statsPie->setLabelsColumn(0);   
    _statsPie->setDataColumn(1);

    // Configure location and type of labels.
    _statsPie->setDisplayLabels(Chart::LabelOption::Outside |
                            Chart::LabelOption::TextLabel |
                            Chart::LabelOption::TextPercentage);

    // Enable a 3D and shadow effect.
    _statsPie->setPerspectiveEnabled(true, 0.2);
    _statsPie->setShadowEnabled(true);

    _statsPie->setExplode(0, 0.3);  
    _statsPie->resize(800, 300);    
    _statsPie->setMargin(10, Side::Top | Side::Bottom); 
    _statsPie->setMargin(WLength::Auto, Side::Left | Side::Right); 
    _statsPie->hide();
}

AppGomoku::~AppGomoku(){
    delete _joueuractuel;
    delete _gagnant;
    delete _boardWidget;
    delete _statsPie;
}

void AppGomoku::restart() {
    _joueuractuel->show();
    _gagnant->show();
    _boardWidget->show();
    _statsPie->hide();
    for(int i=0; i<_board.ni();i++)
        for(int j=0; j<_board.nj();j++)
            _board.set(i,j,PLAYER_NONE);
    if(_board.getJoueuractuel()==PLAYER_BLACK)
        _board.switchJoueuractuel();
    actualiser();
}

void AppGomoku::afficherStats(){
    _joueuractuel->hide();
    _gagnant->hide();
    _boardWidget->hide();
    _statsPie->show();
}

void AppGomoku::actualiser() {
    switch (_board.getJoueuractuel()) {
    case PLAYER_BLACK:
        _joueuractuel->setText(WString("Joueur actuel: Joueur noir"));
        break;
    case PLAYER_WHITE:
        _joueuractuel->setText(WString("Joueur actuel: Joueur blanc"));
        break;
    default:
        break;
    }
    Dbo::Transaction transaction(_session);
    auto _model = createModel();
    switch (_board.getGagnant()) {

    case PLAYER_NONE:
        _gagnant->setText(WString("Gagnant: None"));
        break;
    case PLAYER_BLACK:
        _gagnant->setText(WString("Gagnant: Joueur noir"));
        _session.add(make_unique<GameResult>(GameResult{WDateTime(now()), 2}));
        _model->setData( 0, 1,_session.query<int>("select count(1) from GameResult")
                         .where("Gagnant = ?").bind("2"));
        break;
    case PLAYER_WHITE:
        _gagnant->setText(WString("Gagnant: Joueur blanc"));
        _session.add(make_unique<GameResult>(GameResult{WDateTime(now()), 1}));
        _model->setData( 1, 1,_session.query<int>("select count(1) from GameResult")
                         .where("Gagnant = ?").bind("1"));
        break;
    case PLAYER_DRAW:
        _gagnant->setText(WString("Gagnant: Egalité"));
        _session.add(make_unique<GameResult>(GameResult{WDateTime(now()), 0}));
        _model->setData( 2, 1,_session.query<int>("select count(1) from GameResult")
                         .where("Gagnant = ?").bind("0"));
        break;
    default:
        break;
    }
    _statsPie->setModel(_model);
    _boardWidget->update();
}

double AppGomoku::getStatBlack(){
    Dbo::Transaction transaction(_session);
    if(_session.query<int>("select count(1) from GameResult")!=0)
        return _session.query<int>("select count(1) from GameResult")
            .where("Gagnant = ?").bind("2")/(double)_session.query<int>("select count(1) from GameResult")*100;
    else return 0.0;
}

double AppGomoku::getStatWhite(){
    Dbo::Transaction transaction(_session);
    if(_session.query<int>("select count(1) from GameResult")!=0)
        return _session.query<int>("select count(1) from GameResult")
            .where("Gagnant = ?").bind("1")/(double)_session.query<int>("select count(1) from GameResult")*100;
    else return 0.0;
}

double AppGomoku::getStatDraw(){
    Dbo::Transaction transaction(_session);
    if(_session.query<int>("select count(1) from GameResult")!=0)
        return _session.query<int>("select count(1) from GameResult")
            .where("Gagnant = ?").bind("0")/(double)_session.query<int>("select count(1) from GameResult")*100;
    else return 0.0;
}

std::shared_ptr<Wt::WStandardItemModel> AppGomoku::createModel(){
    auto _model = std::make_shared<WStandardItemModel>();
    _model->setItemPrototype(cpp14::make_unique<WStandardItem>());
    _model->insertColumns(_model->columnCount(), 2);
    _model->setHeaderData(0, WString("Gagnant"));
    _model->setHeaderData(1, WString("Gagner"));
    _model->insertRows(_model->rowCount(), 3);
    Dbo::Transaction transaction(_session);
    int countB = _session.query<int>("select count(1) from GameResult").where("Gagnant = ?").bind("2");
    int countW = _session.query<int>("select count(1) from GameResult").where("Gagnant = ?").bind("1");
    int countD = _session.query<int>("select count(1) from GameResult").where("Gagnant = ?").bind("0");
    _model->setData( 0, 0, WString("Noir"));
    _model->setData( 0, 1,countB);
    _model->setData( 1, 0, WString("Blanc"));
    _model->setData( 1, 1,countW);
    _model->setData( 2, 0, WString("Draw"));
    _model->setData( 2, 1,countD);
    return _model;
}
