#ifndef GOMOKU_HPP
#define GOMOKU_HPP
#include "Board.hpp"
#include "Random.hpp"
#include "Gomoku.hpp"
/**
 * @brief Intelligence articifielle pour le jeu de Gomoku
 * basé sur un algorithme Monte Carlo.
 */
class Gomoku
{
private:
    Board &_board; /// Jeu d'application de l'IA
public:
    /**
     * @brief MonteCarlo
     * @param board
     */
    Gomoku(Board &board);
    /**
     * @brief Permet à l'IA de choisir les coups à jouer
     * @param i Indice de ligne choisi par l'IA
     * @param j Indice de colonne choisi par l'IA
     */
    void whatToPlay(int &i ,int &j);
    /**
     * @brief Permet de lancer un nombre de partie avec
     * la Board passée en paramètre.
     * @param board Jeu de départ
     * @param nbPartie Nombre de parties à jouer
     * @return Retourne la somme les résultats (+1.0 si victoire, +0.5 si égalité)
     */
    double playWith(const Board &board, int nbPartie);

};

#endif // MONTECARLO_HPP
