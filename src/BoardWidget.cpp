#include "BoardWidget.hpp"
#include "AppGomoku.hpp"
#include <sstream>
#include <iostream>
#include <functional>
#include "Gomoku.hpp"

BoardWidget::BoardWidget(AppGomoku & appGomoku, Board &board) :
    Wt::WPaintedWidget(),
    _appGomoku(appGomoku),
    _board(board)
{
    resize(BOARD_WIDTH, BOARD_HEIGHT);
    int pasH = BOARD_WIDTH /board.nj();
    int pasV = BOARD_HEIGHT / board.ni();
    for(int i=0;i<_board.ni();i++) {
        for(int j=0;j<_board.nj();j++) {
            std::unique_ptr<Wt::WAbstractArea> r(new Wt::WRectArea(j*pasH,i*pasV,pasH,pasV));

            r->clicked().connect(std::bind(&BoardWidget::handleClick,this,i,j));
            addArea(std::move(r));

        }
    }

}

void BoardWidget::handleClick(int i,int j) {
    if(_board.getGagnant()==PLAYER_NONE and _board.play(i,j)) {
        Gomoku m(_board);
        _board.switchJoueuractuel();
        int i,j;
        m.whatToPlay(i,j);
        if(_board.getGagnant()==PLAYER_NONE and _board.play(i,j));
        _board.switchJoueuractuel();
        _appGomoku.actualiser();
    }
    return;
}

void BoardWidget::paintEvent(Wt::WPaintDevice * paintDevice) {

    const int hauteur = BOARD_HEIGHT, largeur = BOARD_WIDTH;
    const int nb_lignes = _board.ni(), nb_cols = _board.nj();
    Wt::WPainter painter(paintDevice);
    Wt::WPen pen;

    painter.setBrush(Wt::WBrush(Wt::StandardColor::DarkGreen));
    pen.setColor(Wt::StandardColor::Black);
    pen.setWidth(Wt::WLength(4.0));
    painter.setPen(pen);
    painter.drawRect(0, 0, hauteur, largeur);
    int pasH = largeur /nb_cols;
    int pasV = hauteur / nb_lignes;

    for(int j=0;j<nb_cols;j++) {
        for(int i=0;i<nb_lignes;i++) {
            if(_board.getGagnant()==PLAYER_NONE)
                painter.setBrush(Wt::WBrush(Wt::StandardColor::DarkGreen));
            else
                painter.setBrush(Wt::WBrush(Wt::StandardColor::Magenta));
            painter.drawRect(j*pasH,i*pasV,pasH,pasV);
            Player case_courante = _board.get(i,j);
            if(case_courante != PLAYER_NONE) {
                if(case_courante==PLAYER_BLACK)
                    painter.setBrush(Wt::WBrush(Wt::StandardColor::Black));
                else
                    painter.setBrush(Wt::WBrush(Wt::StandardColor::White));

                double x_mid = (double)j*pasH+((double)pasH*0.1), y_mid = (double)i*pasV+((double)pasV*0.1);
                double rayon = 0.8*pasV;
                painter.drawEllipse(x_mid,y_mid,rayon,rayon);
            }
        }
    }
}



