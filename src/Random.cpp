#include "Random.hpp"

Random::Random() :
    _engine(std::random_device{}()),
    _distribution(0, 1)
{}

int Random::operator()(int nMax) {
    return std::min(nMax-1, int(nMax*_distribution(_engine)));
}

