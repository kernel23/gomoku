#include "Gomoku.hpp"

Gomoku::Gomoku(Board &board):
    _board(board)
{

}
double Gomoku::playWith(const Board &board, int nbPartie) {
    double score_coup = 0.0;
    for(int l=0;l<nbPartie;l++) {
        Random r;
        Board b2 = board;
        while(b2.getGagnant()==PLAYER_NONE) {
            b2.switchJoueuractuel();
            int i2 = r(b2.ni()), j2 = r(b2.nj());
            while(not b2.play(i2,j2)) {
                i2 = r(b2.ni()); j2 = r(b2.nj());
            }

            b2.switchJoueuractuel();
            if(b2.getGagnant()!= PLAYER_NONE) break;
            i2 = r(b2.ni()); j2 = r(b2.nj());
            while(not b2.play(i2,j2)) {
                i2 = r(b2.ni()); j2 = r(b2.nj());
            }
        }
        if(b2.getGagnant()==PLAYER_BLACK)
            score_coup += 1.0;
        else if(b2.getGagnant()==PLAYER_NONE)
            score_coup += 0.5;
    }
    return score_coup;
}

void Gomoku::whatToPlay(int &i, int &j) {
    const int NB_PARTIES = 1000;
    double score = 0.0;

    // teste pour chaque coup
    for(int i1=0;i1<_board.ni();i1++) {
        for(int j1=0;j1<_board.nj();j1++) {
            Board b1 = _board;

            if(b1.get(i1,j1)!=PLAYER_NONE) {
                continue;
            }
            b1.play(i1,j1);
            // nouvelle une partie
            double score_coup = playWith(b1,NB_PARTIES);


            if(score_coup >= score) {
                score = score_coup;
                i = i1;
                j = j1;
            }
        }
    }

}



