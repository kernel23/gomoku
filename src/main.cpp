#include "Board.hpp"
#include <iostream>
using namespace std;

int main() {
	cout <<"==================================="<<std::endl;
	cout <<" Mohamed Hsaina et Zakaria Fahraoui "<<std::endl;
	cout <<" Group 2 "<<std::endl;
	cout <<"==================================="<<std::endl;
    Board b(5,8);
    b.runText();
    std::cout << b;
}
