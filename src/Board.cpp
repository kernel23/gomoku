#include "Board.hpp"
#include <string>

Board::Board(int ni,int nj)
    :_ni(ni),_nj(nj),_data(ni*nj,PLAYER_NONE),_joueuractuel(PLAYER_WHITE)
{

}

void Board::set(int i, int j, Player p) {
    if(i>=_ni or j>=_nj) throw std::string("Erreur : indice incorrect !");
    else _data[getKey(i,j)] = p;
}

Player Board::get(int i, int j) const {
    if(i>=_ni or j>=_nj) throw std::string("Erreur : indice incorrect !");
    else return _data[getKey(i,j)];
}

Player Board::getJoueuractuel() const{
    return _joueuractuel;
}

int Board::ni() const{
    return _ni;
}
int Board::nj() const{
    return _nj;
}
Player Board::getGagnant() const{
    bool win=false;
    bool egalite = true;
    for(int i=0;i<_ni;i++){
        for(int j=0;j<_nj;j++){
            if(get(i,j)!=PLAYER_NONE){
                if(i <= _ni-5){//si gagner la ligne est possible
                    //check ligne
                    for(int k=1;k<5;k++){
                        if(get(i,j) != get(i+k,j)){
                            win = false;
                            break;
                        }
                        win = true;
                    }
                }
                if(win == true) return get(i,j);
                if(j <= _nj-5){//si gagne la colonne est possible
                    //check colonne
                    for(int k=1;k<5;k++){
                        if(get(i,j) != get(i,j+k)){
                            win = false;
                            break;
                        }
                        win = true;
                    }
                }
                if(win == true) return get(i,j);
                if(i <= _ni-5 && j <= _nj-5){//si 1ere diagonale possible
                    //check diagonale
                    for(int k=1;k<5;k++){
                        if(get(i,j) != get(i+k,j+k)){
                            win = false;
                            break;
                        }
                        win = true;
                    }
                }
                if(win == true) return get(i,j);
                if(i >= 4 && j <= _nj-5){//si 2eme diagonale possible
                    //check diagonale
                    for(int k=1;k<5;k++){
                        if(get(i,j) != get(i-k,j+k)){
                            win = false;
                            break;
                        }
                        win = true;
                    }
                }
                if(win == true) return get(i,j);
            }
            else egalite=false;
            if(win==true) return get(i,j);
        }
    }
    if(egalite == true) return PLAYER_DRAW;
    return PLAYER_NONE;
}

void Board::runText(){
    
    _joueuractuel = PLAYER_WHITE;

    while(true) {
        Player gagnant;
        std::cout << "current: " << getJoueuractuel() << std::endl;
        std::cout << "winner: " << (gagnant = getGagnant()) << std::endl;

        if(gagnant!=PLAYER_NONE) return;

        int pi,pj;
        std::cout << "i j ? ";
        std::cin >> pi >> pj;
        while(not play(pi,pj)) {
            std::cout << "i j ? ";
            std::cin >> pi >> pj;
        }
        for(int i=0;i<_nj;i++) std::cout << " " << i;
        std::cout << std::endl;
        for(int i=0;i<_nj;i++) std::cout << "--";
        std::cout << std::endl;

        std::cout << *this;


        if(getJoueuractuel()==PLAYER_BLACK)
            _joueuractuel=PLAYER_WHITE;
        else _joueuractuel=PLAYER_BLACK;

        std::cout << std::endl << std::endl;
    }


}

int Board::getKey(int i, int j) const{
    return i*_nj+j;
}

std::ostream& operator<<(std::ostream& os,const Board &b){
    for(int i=0;i<b._ni;i++) {
        for(int j=0;j<b._nj;j++) {
            os << " " << b.get(i,j);
        }
        os << std::endl;
    }
    return os;
}

bool Board::play(int i, int j) {
    Player p;
    try {
        p = get(i,j);
    }
    catch(const std::string &e) {
        return false;
    }
    if(p!=PLAYER_NONE) return false;
    try {
        set(i,j,getJoueuractuel());
    }
    catch(const std::string &e) {
        return false;
    }

    return true;
}

void Board::switchJoueuractuel() {
    if(getJoueuractuel()==PLAYER_BLACK)
        _joueuractuel = PLAYER_WHITE;
    else
        _joueuractuel = PLAYER_BLACK;
}
