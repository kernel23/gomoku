#ifndef BOARD_WIDGET_HPP
#define BOARD_WIDGET_HPP


#include <Wt/WPaintedWidget.h>
#include <Wt/WPainter.h>
#include <Wt/WAbstractArea.h>
#include <Wt/WRectArea.h>
#include "Board.hpp"

#define BOARD_SCALE 0.6
#define BOARD_HEIGHT 900*BOARD_SCALE
#define BOARD_WIDTH 1300*BOARD_SCALE
class AppGomoku;

/**
 * @brief Représentation graphique d'un plateau de jeu
 */
class BoardWidget : public Wt::WPaintedWidget {
    private:
        AppGomoku & _appGomoku;
        Board & _board;

    public:
        /**
         * @brief Représentation graphique du plateau de Gomoku
         * @param appGomoku Référence vers l'application principale
         * @param board Référence vers le moteur du jeu
         */
        BoardWidget(AppGomoku & appGomoku, Board &board);

    protected:
        /**
         * @brief Affichage de la grille
         * et des pions.
         * @param paintDevice
         */
        void paintEvent(Wt::WPaintDevice * paintDevice);

    private:
        /**
         * @brief Gestion du clic sur la grille.
         * @param i Indice de ligne
         * @param j Indice de colonne
         */
        void handleClick(int i, int j);
};

#endif

