#ifndef APP_GOMOKU_HPP
#define APP_GOMOKU_HPP

#include "BoardWidget.hpp"


#include <Wt/WApplication.h>
#include <Wt/WText.h>
#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include <Wt/Dbo/WtSqlTraits.h>
#include <Wt/WDateTime.h>
#include <Wt/Chart/WPieChart.h>


struct GameResult {
    Wt::WDateTime date;
    int gagnant;
    template<class Action> void persist(Action & a) {
        Wt::Dbo::field(a, date, "date");
        Wt::Dbo::field(a, gagnant, "Gagnant");
    }
};



/**
 * @brief Application principale
 */
class AppGomoku : public Wt::WApplication {
    private:
        Wt::WText * _joueuractuel;
        Wt::WText * _gagnant;
        Board _board;
        BoardWidget *_boardWidget;
        Wt::Chart::WPieChart *_statsPie;
        Wt::Dbo::Session _session;

    public:
        /**
         * @brief AppGomoku
         * @param env
         */
        explicit AppGomoku(const Wt::WEnvironment & env);

        /**
         * @brief le Destructeur 
         */
        ~AppGomoku();
        /**
         * @brief Redémarrer la partie
         * réinitialiser toutes les cases.
         */
        void restart();

        /**
         * @brief Actualiser l'affichage des infos
         */
        void actualiser();

        /**
         * @brief Donner les statistique de victoire des noirs
         * @return Retourner un pourcentage de victoire
         */
        double getStatBlack();
        /**
         * @brief Donne les statistique de victoire des blancs
         * @return Retourne un pourcentage de victoire
         */
        double getStatWhite();
        /**
         * @brief getStatDraw
         * @return Retourne un pourcentage d'égalité
         */
        double getStatDraw();
        /**
         * @brief Creation du model de donnée statistique
         * @return Un modèle de donnée
         */
        std::shared_ptr<Wt::WStandardItemModel> createModel();
        /**
         * @brief afficher les statistique
         */
        void afficherStats();
};

#endif

