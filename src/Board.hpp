#ifndef BOARD_HPP
#define BOARD_HPP


#include <iostream>
#include <vector>
#include <ostream>
#include "Board.hpp"
enum Player { PLAYER_NONE, PLAYER_WHITE, PLAYER_BLACK, PLAYER_DRAW };

/**
 * @brief Moteur de jeu
 */
class Board {
    private:
        int _ni; /// Nombre de lignes du plateau
        int _nj; /// Nombre de colonnes du plateau
        std::vector<Player> _data; /// Cases du plateau
        Player _joueuractuel; /// Joueur dont c'est le tour
    public:
        /**
         * @publicsection
         * @brief Board
         * @param ni
         * @param nj
         * @brief Constructeur initialise un plateau
         * de ni * nj cases
         */
        Board(int ni, int nj);
        /**
         * @brief accesseur
         * @return Retourne le nombre de lignes
         */
        int ni() const;
        /**
         * @brief accesseur
         * @return Retourne le nombre de colonnes
         */
        int nj() const;
        /**
         * @brief Determination du gagnant de la partie
         * @return PLAYER_WHITE ou PLAYER_BLACK si gagnant
         * PLAYER_DRAW si egalite
         * PLAYER_NONE sinon
         */
        Player getGagnant() const;
        /**
         * @brief Determination du joueur dont c'est le tour
         * @return PLAYER_BLACK ou PLAYER_WHITE
         */
        Player getJoueuractuel() const;

        /**
         * @brief Permet de changer le joueur courant
         * (PLAYER_BLACK <---> PLAYER_WHITE).
         */
        void switchJoueuractuel();
        /**
         * @brief Permet de jouer un coup avec le joueur courant
         * @param i
         * @param j
         * @return true si le coup a été joué
         * false sinon
         */
        bool play(int i, int j);

        /**
         * @brief Lance une partie en affichage console
         */
        void runText();
        /**
         * @brief Recuperer la case d'indices i et j
         * @param i Indice de ligne
         * @param j indice de colonne
         * @return PLAYER_WHITE ou PLAYER_BLACK,
         * PLAYER_NONE si case inoccupée
         */
        Player get(int i, int j) const;
        /**
         * @brief Permet de changer la valeur de la case
         * d'indices i et j
         * @param i Indice de ligne
         * @param j Indice de colonne
         * @param p Valeur de la case (PLAYER_WHITE,PLAYER_BLACK,PLAYER_NONE)
         */
        void set(int i, int j, Player p);
        /**
         * @brief operator <<
         * @param os
         * @param b
         * @return
         */
        friend std::ostream& operator<<(std::ostream &os,const Board &b);
    protected:
        /**
         * @brief Renvoie l'indice de la case dans le tableau
         * en fonction d'une position sur un plateau 2D
         * @param i Indice de ligne
         * @param j Indice de colonne
         * @return Indice dans le tableau
         */
        int getKey(int i, int j) const;
};


#endif // BOARD_HPP
