#include "Board.hpp"
#include <string>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupBoard){};

TEST(GroupBoard,Test_getset) {
    Board b(12,12);
    b.set(1,2,PLAYER_WHITE);
    CHECK_THROWS(std::string,b.set(13,13,PLAYER_BLACK));
    CHECK_EQUAL(b.get(1,2),PLAYER_WHITE);
}

TEST(GroupBoard,Test_Board1){
    Board b(4,4);
    CHECK_EQUAL(b.getJoueuractuel(),PLAYER_WHITE);
    CHECK_EQUAL(b.get(3,3),PLAYER_NONE);
}

TEST(GroupBoard,Test_Board2){
    Board b(4,4);
    b.set(0,0,PLAYER_WHITE);
    CHECK_EQUAL(b.get(0,0),PLAYER_WHITE);
}

TEST(GroupBoard,Test_Board3){
    Board b(4,4);
    b.set(0,0,PLAYER_WHITE);
    CHECK_EQUAL(b.get(0,0),PLAYER_WHITE);
}

TEST(GroupBoard,Test_Board4){
    Board b(5,5);
    b.set(0,0,PLAYER_WHITE);
    b.set(1,0,PLAYER_WHITE);
    b.set(2,0,PLAYER_WHITE);
    b.set(3,0,PLAYER_WHITE);
    b.set(4,0,PLAYER_WHITE);
    CHECK_EQUAL(b.getGagnant(),PLAYER_WHITE);
}

TEST(GroupBoard,Test_Board5){
    Board b(4,4);
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++)
            b.set(i,j,PLAYER_WHITE);
    CHECK_EQUAL(b.getGagnant(),PLAYER_DRAW);
}

